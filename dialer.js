const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');

const http = require('http').Server(app);
const io = require('socket.io')(http);

let bridge = null;
const config = {
    url: 'https://uni-call.fcc-online.pl',
    login: 'focus06',
    password: '0cgdfgwrbl'
};

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());

http.listen(3000, () => {
    console.log('app listening on port 3000');
});

io.on('connection', (socket) => {
    console.log('a user connected')
    socket.on('disconnect', () => {
        console.log('a user disconnected')
    })
    socket.on('message', (message) => {
        console.log('message', message)
    })
    io.emit('message', 'connected!')
})

/*app.get('/call/:number1/:number2', async (req, res) => {
    //dzwonienie wykonuje się: Dialer.call(<number1>,<number2>)
    const number1 = req.params.number1;
    const number2 = req.params.number2;
    Dialer.call(number1, number2);
    bridge = await Dialer.call(number1, number2);
    res.json({ success: true });
})*/

app.post('/call', async (req, res) => {
    const body = req.body;
    console.log(body);

    bridge = await Dialer.call(body.number1, body.number2);
    let oldStatus = null;
    let interval = setInterval(async () => {
        let currentStatus = await bridge.getStatus();
        if (currentStatus !== oldStatus) {
            oldStatus = currentStatus
            io.emit('message', currentStatus)
        }
    }, 500)
    res.json({ success: true });
})

app.get('/status', async (req, res) => {
    //let status = await bridge.getStatus();
    //res.json({ success: true, status: status });

    let status = 'NONE';
    if (bridge !== null) {
        status = await bridge.getStatus();
    }
    res.json({ success: true, status: status })
});